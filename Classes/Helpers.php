<?php

namespace DKM\DivertRequest;

use TYPO3\CMS\Core\Core\Environment;

class Helpers
{
    static public function getConfiguration(): array
    {
        static $configuration = [];
        if(!$configuration) {
            $path = Environment::getConfigPath() . '/divert_request/';
            foreach (\TYPO3\CMS\Core\Utility\GeneralUtility::getFilesInDir($path) as $fileName) {
                $configuration = array_merge_recursive($configuration, include "{$path}{$fileName}");
            };
        };
        return $configuration;
    }


    static public function getConfigurationForEvent($eventClassName)
    {
        $configuration = self::getConfiguration();
        $specificConfiguration = $configuration['events'][$eventClassName];
        $specificConfiguration['conditionSet'] = $configuration['conditionSets'][$specificConfiguration['conditionSet']];
        return $specificConfiguration;
    }

}