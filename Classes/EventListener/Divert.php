<?php

namespace DKM\DivertRequest\EventListener;

use DKM\DivertRequest\Helpers;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Http\PropagateResponseException;

class Divert
{


    /**
     * @throws Exception
     * @throws PropagateResponseException
     */
    public function __invoke(object $event)
    {
        $configuration = Helpers::getConfigurationForEvent(get_class($event));
        $divert = false;
        if($ruleSet = $configuration['conditionSet'] ?? false) {
            foreach ($ruleSet as $rule) {
                if($divert = self::executeRule($rule)) {
                    break;
                };
            }
        }

        if($divert) {
            // Divert to same host name, but without query
            $responseFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
                \Psr\Http\Message\ResponseFactoryInterface::class
            );
            $redirectType = $configuration['redirect']['type'] ?? 'removeQuery';
            switch ($redirectType) {
                case 'redirectToUrl':
                    if(!$redirectUrl = $configuration['redirect']['redirectUrl'] ?? false) {
                        $redirectUrl = (string)$GLOBALS['TYPO3_REQUEST']->getUri()->withQuery('');
                    }
                    break;
                default:
                    $redirectUrl = (string)$GLOBALS['TYPO3_REQUEST']->getUri()->withQuery('');
                    break;
            }

            $response = $responseFactory
                ->createResponse()
                ->withAddedHeader('location', $redirectUrl);

            throw new \TYPO3\CMS\Core\Http\PropagateResponseException($response);

        }
    }

    /**
     * @throws Exception
     */
    static public function executeRule($rule): bool
    {
        switch ($rule['type'] ?? false) {
            case 'requestMethod':
                return self::requestMethod($rule['comparisonOperator'], $rule['value']);
                break;
            case 'userAgent':
                return self::userAgent($rule['value'] ?? null, $rule['pattern'] ?? null);
                break;
            default:
                return false;
        }
    }

    /**
     * @param $comparator
     * @param $value
     * @return bool
     * @throws Exception
     */
    static public function requestMethod($comparator, $value): bool
    {
        if(!ctype_alpha($value)) throw new Exception('The $value can only be alpha numeric');
        if(!!str_replace(['!','=', '<', '>'], '', $comparator)) throw new Exception('The $comparator can only be comparison operators');
        $requestMethod = $GLOBALS['TYPO3_REQUEST']->getMethod();
        return !!eval("return '$requestMethod' $comparator '$value';");
    }

    /**
     * @param $value
     * @param $pattern
     * @return false|int
     */
    static public function userAgent($value = null, $pattern = null)
    {
        $userAgent = null;
        if($request = $GLOBALS['TYPO3_REQUEST'] ?? false) {
            $userAgent = $request->getHeader('user-agent')[0] ?? null;
        }
        if(!$userAgent) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'] ?? false;
        }
        if($userAgent) {
            if($value && !$pattern) {
                $pattern = "/{$value}/i";
            }
            if($pattern) {
                return preg_match($pattern, $userAgent);
            }
        }
        return false;
    }
}