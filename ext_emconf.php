<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => '3S - dkmweb4',
    'description' => 'dkmweb4 specific files',
    'category' => 'misc',
    'shy' => 0,
    'version' => '0.0.2',
    'dependencies' => 'cms,extbase,fluid,templavoila',
    'conflicts' => '',
    'priority' => '',
    'loadOrder' => '',
    'module' => '',
    'state' => 'experimental',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author' => 'Stig Nørgaard Færch',
    'author_email' => 'snf@dkm.dk',
    'author_company' => 'Danish Church Mediacenter'
);
