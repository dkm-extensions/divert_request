<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Core\Core\Environment;

return function (ContainerConfigurator $container, ContainerBuilder $containerBuilder) {
    $services = $container->services();
    $path = Environment::getConfigPath() . '/divert_request/';
    $configuration = [];
    foreach (\TYPO3\CMS\Core\Utility\GeneralUtility::getFilesInDir($path) as $fileName) {
        $configuration = array_merge_recursive($configuration, include "{$path}{$fileName}");
    };
    foreach ($configuration['events'] as $className => $_) {
        $name = \TYPO3\CMS\Core\Utility\GeneralUtility::camelCaseToLowerCaseUnderscored(str_replace('\\', '__', 'In2code\Femanager\Event\BeforeUserConfirmEvent')) . '__divert';
        $services->set($name)
            ->class(\DKM\DivertRequest\EventListener\Divert::class)
            ->tag(
              'event.listener',
              [
                  'identifier' => $name,
                  'event' => $className
              ]
            );
    }
};
