<?php
/**
 * Content of example should be placed here /config/divert_request/fe_manager.php
 */
return [
    'conditionSets' => [
        'conditionSet1' => [
            [
                'type' => 'requestMethod',
                'comparisonOperator' => '!==',
                'value' => 'GET'
            ],
// It's also possible to match on user agent. Either use value or regex pattern:
//            [
//                'type' => 'userAgent',
//                'value' => 'postman'
//                'pattern' => '/postman/i'
//            ]
        ]
    ],
    'events' => [
        \In2code\Femanager\Event\BeforeUserConfirmEvent::class => [
            'conditionSet' => 'conditionSet1',
            'redirect' => ['type' => 'removeQuery']
            // This would redirect to typo3.com instead
//            'redirect' => ['type' => 'redirectToUrl', 'redirectUrl' => 'https://www.typo3.com']
        ]
    ]
];
